import axios from 'axios';

const apiUrl = 'http://localhost:3001/users/';

const GET_ERRORS = 'GET_ERRORS';

export const registerUser = (user, history) => dispatch => {
    axios.post(`${apiUrl}/register`, user)
        .then(res => history.push('/verify'))
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
};

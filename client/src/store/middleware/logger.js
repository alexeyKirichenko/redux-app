export  const logger = store => next => action => {
    console.log('previous', store.getState());
    console.log('action',next(action));
    console.log('next', store.getState());
};
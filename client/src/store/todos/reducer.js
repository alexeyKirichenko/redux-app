import {error, success} from "redux-saga-requests";

const initialState = {
    items: [],
    isError: false,
    isFetching: false,
};

const todos = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_TODOS':
            return { ...state,
                    isFetching: true
                    };

        case success('FETCH_TODOS'):
            return {...state,
                    isFetching: false,
                    isError: false,
                    items: action.payload.data
                    };

        case error('FETCH_TODOS'):
            return {...state,
                    isFetching: false,
                    isError: true
                    };

        case 'POST_TODO':
            return  { ...state };

        case success('POST_TODO'):
            return  { ...state,
                    isFetching: false,
                    isError: false,
                    items: [...state.items, action.payload.data]
                    };

        case error('POST_TODO'):
            return  { ...state,
                    isFetching: false,
                    isError: true
                    };

        case 'DELETE_TODO':
            return { ...state };

        case success('DELETE_TODO'):
            return {...state,
                    isFetching: false,
                    isError: false,
                    items: state.items.filter((item) => item._id !== action.meta.id)
            };

        case error('DELETE_TODO'):
            return {...state,
                    isFetching: false,
                    isError: true
            };

        case 'CHECK_TODO':
            return { ...state };

        case success('CHECK_TODO'):
            return {...state,
                    isFetching: false,
                    isError: false,
                    items: state.items.map(item => item._id === action.meta.id ?
                    {...item, completed: !item.completed} : item)
            };

        case error('CHECK_TODO'):
            return {...state,
                    isFetching: false,
                    isError: true
                    };

        default:
            return state;
    }
};

export default todos;

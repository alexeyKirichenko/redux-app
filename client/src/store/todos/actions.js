import uuidv4 from "uuid4";

const apiUrl = 'http://localhost:3001/';

const FETCH_TODOS = 'FETCH_TODOS';
export const fetchItems = () => ({
  type: FETCH_TODOS,
  payload: {
      request: {
            method: 'GET',
            url: `${apiUrl}todos`
      }
    }
});

const POST_TODO = 'POST_TODO';
export  const postItem = value => ({
    type: POST_TODO,
    payload: {
        request: {
            method: 'POST',
            url: `${apiUrl}todos`,
            data: {
                id: uuidv4(),
                value,
                completed: false
            }
        }
    }
});

const DELETE_TODO = 'DELETE_TODO';
export const deleteItem = id => ({
    type: DELETE_TODO,
    payload: {
        request: {
            method: 'DELETE',
            url: `${apiUrl}todos/${id}`,
        }
    },
    meta: { id }
});

const CHECK_TODO = 'CHECK_TODO';
export const checkItem = todo => ({
    type: CHECK_TODO,
    payload: {
        request: {
            method: 'PUT',
            url: `${apiUrl}todos/${todo._id}`,
            data: {...todo, completed: !todo.completed}
        }
    },
    meta: { id: todo._id }
});

import {createStore, applyMiddleware, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';
import saga from './saga'
import {requestsPromiseMiddleware} from "redux-saga-requests";
import thunkMiddleware from "redux-thunk";
import todos from "./todos/reducer";
import {logger} from "./middleware/logger";
import errorReducer from "./errors/reducer";
import reducer from "./auth/reducer";

const rootReducer = combineReducers({
    todos,
    errors: errorReducer,
    auth: reducer
});

const sagaMiddleware = createSagaMiddleware();

const index = createStore(rootReducer, applyMiddleware(requestsPromiseMiddleware(), thunkMiddleware, sagaMiddleware, logger));

sagaMiddleware.run(saga);

export default index;

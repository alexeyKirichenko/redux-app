import React, {Component} from 'react';
import {Provider} from "react-redux";
import index from '../store/index'
import Header from './Header';
import Home from './Home'
import Register from './Register'
import Login from './Login'
import { BrowserRouter as Router, Route } from "react-router-dom";
import jwt_decode from 'jwt-decode';
import {logoutUser, setCurrentUser} from "../store/auth/action";
import setAuthToken from "../setAuthToken";
import store from '../store/index'
import {Verify} from "./Verify";




class App extends Component {

    componentDidMount() {
        if(localStorage.jwtToken) {
            setAuthToken(localStorage.jwtToken);
            const decoded = jwt_decode(localStorage.jwtToken);
            store.dispatch(setCurrentUser(decoded));

            const currentTime = Date.now() / 1000;
            if(decoded.exp < currentTime) {
                store.dispatch(logoutUser());
                window.location.href = '/login'
            }
        }
    }

    render() {
        return (
            <Provider store={index}>
                <div>
                    <Router>
                        <div>
                        <Header/>
                        <Route exact path="/" component={Home} />
                        <Route path="/login" component={Login} />
                        <Route path="/register" component={Register} />
                        <Route path="/verify" component={Verify} />
                        </div>
                    </Router>
            </div>
            </Provider>
        )
    }
}

export default App;

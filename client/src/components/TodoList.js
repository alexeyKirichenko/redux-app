import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {checkItem, deleteItem, fetchItems} from "../store/todos/actions";
import deleteButton from '../assets/icon-delete.png'
import styled from "styled-components";

const Todo = styled.p(props => ({
    fontSize: '20px',
    margin: '0',
    textDecoration:  props.completed ? 'line-through' : 'none',
    color:  props.completed ? '#d9d9d9': '#fff',
}));

const List = styled.li({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '2px',
    backgroundColor: '#4696e5'
});

const ContainerList = styled.ul({
    margin: '0 50px',

});

const LoadingTodo = styled.div({
    textAlign: 'center'
});

const NoTodos = styled.div`
    text-align: center;
    font-size: 40px;
    color: #828282;
`;



class TodoList extends Component {

    componentDidMount() {
        this.props.fetchItems();
    }

    deleteTodo = (id) => {
        this.props.deleteItem(id)
    };

    checkTodo = (todo) => {
        this.props.checkItem(todo);
    };

    render() {
        return (
                <ContainerList className="list-group">

                    {this.props.isFetching ?  <LoadingTodo>Loading...</LoadingTodo> :
                        this.props.todos.map(todo => (
                        <List key={todo._id}
                              className="list-group-item"
                        >

                            <input type="checkbox"
                                   checked={todo.completed}
                                   onChange={() => this.checkTodo(todo)}
                            />

                            <Todo completed={todo.completed}>
                                {todo.value}
                            </Todo>

                            <button onClick={() => this.deleteTodo(todo._id)}
                                    className="btn btn-link"
                            >
                                <img src={deleteButton}
                                     alt='delete'/>
                            </button>
                        </List>
                        ))
                    }
                    {this.props.todos.length < 1 ? <NoTodos>You don't have a todos!</NoTodos> : false}
                </ContainerList>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        todos: state.todos.items,
        isFetching: state.todos.isFetching
    }
};

const mapActionsToProps = (dispatch) => {
    return {
        deleteItem: (id) => dispatch(deleteItem(id)),
        checkItem: (todo) => dispatch(checkItem(todo)),
        fetchItems: () => dispatch(fetchItems())
    }
};

export default connect(mapStateToProps, mapActionsToProps)(TodoList);

import React, {Component} from 'react';
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import styled from "styled-components";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";

const RightLine = styled.div({
    margin: '0 auto',
    height: '5px',
    width: '300px',
    backgroundColor: '#4696e5',
    borderRadius: '10px',
    marginBottom: '20px'
});

const Title = styled.h1({
    textAlign: 'center',
    color: '#4696e5',
    fontSize: '70px',
    fontWeight: 'bold',
    marginBottom: '30px',
});

class Home extends Component {
    render() {
        return(
            this.props.auth.isAuthenticated ?
            <div>
                <Title>Todo App</Title>
                < AddTodo/>
                < RightLine></RightLine>
                <TodoList/>
            </div> : <Redirect to='/login'> </Redirect>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(mapStateToProps, null)(Home)
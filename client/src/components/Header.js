import React, {Component} from 'react';
import styled from 'styled-components';
import {Link, withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import {logoutUser} from "../store/auth/action";

const Logo = styled.p`
    color: #fff;
    font-size: 30px;
    font-style: italic;
        & :hover {
        text-decoration: none;
        font-weight: bold;
        }  
        & :visited {
        color: #fff;
        }     
`;


const ContainerHeader = styled.div`
    display: flex;
    padding: 20px 50px;
    justify-content: space-between;
    align-items: center;
    background-color: #4696e5;
    margin-bottom: 50px;
`;

const NavList = styled.ul`
    display: flex;
    margin: 0;
    list-style: none;
`;

const ListElem = styled.li`
    margin: 0 20px;
    color: #fff;
    & :hover {
    text-decoration: none;
    border-bottom: 2px solid #fff;
}
& :visited {
    color: #fff;
}`;

const Logout = styled.p`
    color: #fff;
    cursor: pointer;
`;

class Header extends Component {

    onLogout = (event) => {
        event.preventDefault();
        this.props.logoutUser(this.props.history);
    };

    render () {
        const { isAuthenticated } = this.props.auth;
        const LogOut = (
            <NavList>
                <Logout onClick={this.onLogout}>
                    Logout
                </Logout>
            </NavList>
        );
        const authLinks = (
            <NavList>
                <ListElem >
                    <Link to="/register">Sign Up</Link>
                </ListElem>
                <ListElem >
                    <Link to="/login">Sign In</Link>
                </ListElem>
            </NavList>
        );
        return (
            <ContainerHeader>
                <Logo><Link to='/'>Home</Link></Logo>
                <nav>
                    {isAuthenticated ? LogOut : authLinks }
                </nav>
            </ContainerHeader>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(mapStateToProps, { logoutUser })(withRouter(Header));
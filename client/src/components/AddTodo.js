import React, {Component} from 'react';
import {postItem} from '../store/todos/actions';
import {connect} from "react-redux";
import styled from 'styled-components';

const Form = styled.form({
    padding: '0 50px 0px 50px',
});

const AddButton = styled.button({
    display: 'flex',
    margin: '0 auto',
    marginBottom: '20px'
});

const AddInput = styled.input({
    marginBottom: '30px'
});

class AddTodo extends Component {

    state = {
        value: ''
    };

    handleValue = (event) => {
        this.setState({
            value: event.target.value
        })
    };

    submitForm = (event) => {
        event.preventDefault();
        this.props.dispatch(postItem(this.state.value));
        this.setState({
            value: ''
        });
    };

    render() {
        return (
            <Form onSubmit={this.submitForm}
            >

                <AddInput type="text"
                       value={this.state.value}
                       onChange={this.handleValue}
                       className="form-control form-control-lg"
                       placeholder='Enter task...'
                       required
                />

                <AddButton type="submit"
                        className="btn btn-outline-primary btn-lg"
                >
                    Add Task
                </AddButton>
            </Form>

        )
    }
}

export default connect()(AddTodo);

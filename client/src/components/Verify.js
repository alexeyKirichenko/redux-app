import React from 'react';
import styled from 'styled-components';
import {Link} from "react-router-dom";

const VerifyContainer = styled.div`
   text-align: center;
    margin: 20px 50px; 
`;

export const Verify = () => {
  return (
      <VerifyContainer className="alert alert-primary" >
          <h4 className="alert-heading">Well done!</h4>
          <p>Thank you for using our application. Please verify your email address</p>
          <p className="mb-0"><Link to='/login'>Sing In</Link></p>
      </VerifyContainer>
  )
};
import React, {Component} from 'react';
import styled from 'styled-components';
import {loginUser} from "../store/auth/action";
import {connect} from "react-redux";
import classnames from 'classnames';

const ContainerLogin = styled.div({
    width: '500px',
    margin: '0 auto'
});

const SingIn = styled.p({
    textAlign: 'center',
    color: '#4696e5',
    fontSize: '40px'
});

const Label = styled.p({
    color: '#4696e5'
});

class Login extends Component {

    state = {
      email: '',
      password: '',
      errors: {}
    };


    handleValue = (event) => {
      this.setState({
          [event.target.name]: event.target.value
      })
    };

    handleSubmit = (event) => {
      event.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password,
        };
        console.log(user);
        this.props.loginUser(user, this.props.history);
    };

    componentDidMount() {
        if(this.props.auth.isAuthenticated) {
            this.props.history.push('/');
        }
    }

    componentWillReceiveProps(nextProps) {
        // if(nextProps.auth.isAuthenticated) {
        //     this.props.history.push('/')
        // }
        if(nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    render() {
        const {errors} = this.state;
        return (
            <ContainerLogin>
                <SingIn>Sing in</SingIn>
            <form onSubmit={ this.handleSubmit }>
                <div className="form-group">
                    <Label>Email address</Label>
                    <input type="email"
                           name='email'
                           className={classnames('form-control', {
                               'is-invalid': errors.email
                           })}
                           aria-describedby="emailHelp"
                           placeholder="Enter email"
                           value={this.state.email}
                           onChange={ this.handleValue } />
                    {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
                </div>

                <div className="form-group">
                    <Label>Password</Label>
                    <input type="password"
                           name='password'
                           className={classnames('form-control', {
                               'is-invalid': errors.password
                           })}
                           placeholder="Password"
                           value={this.state.password}
                           onChange={this.handleValue } />
                    {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
                </div>
                <button type="submit" className="btn btn-primary btn-lg btn-block">Sing In</button>
            </form>

            </ContainerLogin>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    errors: state.errors
});

export  default connect(mapStateToProps, { loginUser })(Login)
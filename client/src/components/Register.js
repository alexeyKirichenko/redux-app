import React, {Component} from 'react';
import styled from 'styled-components';
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {registerUser} from "../store/errors/actions";
import classnames from 'classnames';

const ContainerRegister = styled.div({
    width: '700px',
    margin: '0 auto'
});

const SingUp = styled.p({
    textAlign: 'center',
    color: '#4696e5',
    fontSize: '40px'
});

const Label = styled.p({
    color: '#4696e5'
});

const Small = styled.small({
    display: 'flex',
    justifyContent: 'center',
    marginTop: '5px'
});


class Register extends Component {

    state = {
      username: '',
      email: '',
      password: '',
      country: '',
      city: '',
      age: '',
      errors: {}
    };


    handleValue = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const user = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            country: this.state.country,
            city: this.state.city,
            age: this.state.age
        };
        this.props.registerUser(user, this.props.history);
    };

    componentWillReceiveProps(nextProps) {
        if(nextProps.auth.isAuthenticated) {
            this.props.history.push('/')
        }
        if(nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    componentDidMount() {
        if(this.props.auth.isAuthenticated) {
            this.props.history.push('/');
        }
    }

    render() {
        const { errors } = this.state;
        return (
            <ContainerRegister>
                <SingUp>Sing Up</SingUp>
                <form onSubmit={ this.handleSubmit }>

                    <div className="form-group">
                        <Label>Username</Label>
                        <input type="text"
                               name='username'
                               className={classnames('form-control', {
                                   'is-invalid': errors.username
                               })}
                               placeholder="Enter your username ..."
                               value={ this.state.username }
                               onChange={ this.handleValue } />
                        {errors.username && (<div className="invalid-feedback">{errors.username}</div>)}
                    </div>

                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <Label>Email</Label>
                            <input type="email"
                                   name='email'
                                   className={classnames('form-control', {
                                       'is-invalid': errors.email
                                   })}
                                   placeholder="Email"
                                   value={ this.state.email }
                                   onChange={ this.handleValue } />
                            {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
                        </div>

                        <div className="form-group col-md-6">
                            <Label>Password</Label>
                            <input type="password"
                                   name='password'
                                   className={classnames('form-control', {
                                       'is-invalid': errors.password
                                   })}
                                   placeholder="Password"
                                   value={ this.state.password }
                                   onChange={ this.handleValue } />
                            {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
                        </div>
                    </div>

                    <div className="form-group">
                        <Label>Country</Label>
                        <input type="text"
                               name='country'
                               className={classnames('form-control', {
                                   'is-invalid': errors.country
                               })}
                               placeholder="Enter your country ..."
                               value={ this.state.country }
                               onChange={ this.handleValue } />
                        {errors.country && (<div className="invalid-feedback">{errors.country}</div>)}
                    </div>

                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <Label>City</Label>
                            <input type="text"
                                   name='city'
                                   className={classnames('form-control', {
                                       'is-invalid': errors.city
                                   })}
                                   placeholder="Enter your city ..."
                                   value={ this.state.city }
                                   onChange={ this.handleValue } />
                            {errors.city && (<div className="invalid-feedback">{errors.city}</div>)}
                        </div>

                        <div className="form-group col-md-4">
                            <Label>Sex</Label>
                            <select className="form-control" defaultValue>
                                <option defaultValue>Male</option>
                                <option>Female</option>
                            </select>
                        </div>

                        <div className="form-group col-md-2">
                            <Label >Age</Label>
                            <input type="text"
                                   name='age'
                                   className={classnames('form-control', {
                                       'is-invalid': errors.age
                                   })}
                                   placeholder="Age ..."
                                   value={ this.state.age }
                                   onChange={ this.handleValue } />
                            {errors.age && (<div className="invalid-feedback">{errors.age}</div>)}
                        </div>

                    </div>
                    <button type="submit"
                            className="btn btn-primary btn-lg btn-block"
                    >
                        Sign Up
                    </button>
                    <Small className="text-muted">
                        Are you have account? <Link to='/login'>Sing In</Link>
                    </Small>
                </form>
            </ContainerRegister>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default  connect(mapStateToProps,{registerUser})(withRouter(Register))
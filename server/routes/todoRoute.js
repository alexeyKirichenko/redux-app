import express from 'express';
import * as todoController from '../controllers/todoController';

const router = express.Router();
router.route('/')
    .get(todoController.getTodos)
    .post(todoController.addTodo);


router.route('/:id')
    .delete(todoController.deleteTodo)
    .put(todoController.checkTodo);

export default router;
import express from 'express';
import * as authController from '../controllers/authConroller';
import passport from 'passport'
import jwt from 'jsonwebtoken'

const router = express.Router();
router.route('/register')
    .post(authController.registerUser);

router.route('/login')
    .post(authController.loginUser)
    .get(authController.log, passport.authenticate('jwt', { session: false }));

export default router;
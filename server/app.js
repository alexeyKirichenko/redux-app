import express from 'express';
import bodyParser from 'body-parser';
import todoRoute from './routes/todoRoute';
import mongoose from 'mongoose';
import db from './db';
import logger from 'morgan';
import cors from 'cors';
import authRoute from './routes/authRoute';
const passport = require('passport');

const app = express();
app.use(passport.initialize());
require('./passport')(passport);


app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const port = process.env.PORT || 3001;

app.get('/', (req,res) =>
    res.end('Server is working')
);

app.use('/todos', todoRoute);
app.use('/users', authRoute);

mongoose.connect(db.DB, { useNewUrlParser: true })
    .then(
        () => {console.log('Database is connected') },
        err => { console.log('Can not connect to the database'+ err)}
    );

app.listen(port, () => {
    console.log(`Server running on port ${port}`)
});

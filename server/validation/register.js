import validator from 'validator';
import isEmpty from '../validation/isEmpty';

const validateRegister = (data) => {
    const errors = {};
    data.username = !isEmpty(data.username) ? data.username : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    data.age = !isEmpty(data.age) ? data.age : '';
    data.city = !isEmpty(data.city) ? data.city : '';
    data.country = !isEmpty(data.country) ? data.country : '';

    if(!validator.isLength(data.username, { min: 2, max: 30 })) {
        errors.username = 'Name must be between 2 to 30 chars';
    }

    if(validator.isEmpty(data.username)) {
        errors.name = 'Name field is required';
    }

    if(!validator.isEmail(data.email)) {
        errors.email = 'Email is invalid';
    }

    if(validator.isEmpty(data.email)) {
        errors.email = 'Email is required';
    }

    if(!validator.isLength(data.password, {min: 6, max: 30})) {
        errors.password = 'Password must have 6 chars';
    }

    if(validator.isEmpty(data.password)) {
        errors.password = 'Password is required';
    }

    if(validator.isEmpty(data.age)) {
        errors.age = 'Age is required'
    }

    if(validator.isEmpty(data.city)) {
        errors.city = 'City is required'
    }

    if(validator.isEmpty(data.country)) {
        errors.country = 'Country is required'
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
};

export default validateRegister;
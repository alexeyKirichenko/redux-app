import mongoose from 'mongoose';

const Schema =  mongoose.Schema;

const todoSchema = new Schema({
    value: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        default: false
    }
});

export default mongoose.model('todos', todoSchema);
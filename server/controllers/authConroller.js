import validateRegister from "../validation/register";
import UserSchema from "../model/userSchema";
import bcrypt from "bcryptjs";
import validateLogin from "../validation/login";
import jwt from 'jsonwebtoken';
import nodemailer from 'nodemailer'

export const registerUser = (req, res) => {
    const { errors, isValid } = validateRegister(req.body);
    if(!isValid) {
        return res.status(400).json(errors);
    }
    UserSchema.findOne({ email: req.body.email })
        .then(user => {
            if(user) {
                return res.status(400).json({
                email: 'Email already exists'
            });
        } else {
            const newUser = new UserSchema({
                username: req.body.username,
                email: req.body.email,
                password: req.body.password,
                age: req.body.age,
                city: req.body.city,
                country: req.body.country,
            });

            bcrypt.genSalt(10, (error, salt) => {
                if (error) {
                    console.error('There was an error: ', error);
                } else {
                    bcrypt.hash(newUser.password, salt, (error, hash) => {
                        if(error) {
                            console.error('There was an error: ', error);
                        } else {
                            newUser.password = hash;
                            newUser.save()
                                .then(user => {
                                    res.json(user);
                                    const transporter = nodemailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: 'web.developer1452@gmail.com',
                                            pass: 'kyrychenko97'
                                        }
                                    });

                                    const mailOptions = {
                                        from: 'Todo Application <web.developer1452@gmail.com>',
                                        to: `${newUser.email}`,
                                        subject: 'Please, confirmed your email address!',
                                        html: '<p>Click at this link and activate your account in app:</p>'
                                    };

                                    transporter.sendMail (mailOptions, function (err, info) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log (info);
                                        }
                                    });
                                });
                        }
                    });
                }
            });
        }
    });
};


export const loginUser = (req, res) => {

    const { errors, isValid } = validateLogin(req.body);

    if(!isValid) {
        return res.status(400).json(errors);
    }

    const email = req.body.email;
    const password = req.body.password;

    UserSchema.findOne({email})
        .then(user => {
            if(!user) {
                errors.email = 'User not found';
                return res.status(404).json(errors);
            }

            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    if(isMatch) {
                        const payload = {
                            id: user.id,
                            name: user.name,
                        };
                        jwt.sign(payload, 'secret', {
                            expiresIn: 3600
                        }, (err, token) => {
                            if(err) {
                                console.error('There is some error in token', err);
                            } else {
                                res.json({
                                    success: true,
                                    token: `${token}`
                                });
                            }
                        });
                } else {
                    errors.password = 'Incorrect Password';
                    return res.status(400).json(errors);
                }
            });
    });
};

export const log =  (req, res) => {
    return res.json({
        id: req.user.id,
        name: req.user.name,
        email: req.user.email
    });
};


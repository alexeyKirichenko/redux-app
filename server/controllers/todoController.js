import Todo from '../model/todoSchem';

export const getTodos = (req,res) => {
  Todo.find().exec((error,todos) => {
      if(error){
          return res.json({'success':false,'message':`Error: ${error}`});
      }
      return res.json(todos);
  });
};

export const addTodo = (req, res) => {
  const newTodo =  new Todo(req.body);
  newTodo.save((error, todo) => {
      if(error){
          return res.json({'success':false,'message':`Error: ${error}`});
      }
      return res.json(todo);
  })
};

export const deleteTodo = (req,res) => {
    Todo.findByIdAndRemove(req.params.id, (error,todo) => {
        if(error){
            return res.json({'success':false,'message':`Error: ${error}`});
        }
        return res.json({'success':true,'message': `${todo.value} +  deleted successfully`});
    })
};


export const checkTodo = (req,res) => {
    Todo.findOneAndUpdate({ _id:req.body._id }, req.body, { new:true }, (err,todo) => {
        if(err){
            return res.json({'success':false,'message':`Some Error: ${err}`});
        }
        console.log(todo);
        return res.json(todo);
    })
};